extends Node2D

func _ready():
	var number_of_buttons = 15
	
	for i in number_of_buttons:
		var button = preload("res://haptic_button.tscn").instance()
		add_child(button)
		
		var angle = i / float(number_of_buttons) * TAU
		var width = min(get_viewport_rect().size.x, get_viewport_rect().size.y)
		var center = get_viewport_rect().size / 2.0
		button.position = center + Vector2(cos(angle) * width/3, sin(angle) * width/3)
