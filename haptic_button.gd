extends Area2D

onready var haptic_mouse = get_tree().root.find_node("HapticMouse", true, false)
onready var sprite = get_node('Sprite')

var is_target
var next = null

func set_target():
	get_tree().call_group_flags(SceneTree.GROUP_CALL_REALTIME, "Buttons", "set_not_target")
	is_target = true
	modulate = Color.red

func set_not_target():
	is_target = false
	modulate = Color.white

func _ready():
	set_target()
	connect("mouse_entered", self, "on_mouse_entered")
#	connect("mouse_exited", self, "on_mouse_exited")

#func _input_event(viewport, event, shape_idx):
#	if event is InputEventMouseButton and event.pressed:
#		on_mouse_clicked()

func on_mouse_entered():
	haptic_mouse.vibrate()

#func on_mouse_exited():
#	pass

#func on_mouse_clicked():
#	if is_target:
#		if next != null:
#			next.set_target()
