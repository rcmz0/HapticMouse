extends Button

onready var haptic_mouse = get_tree().root.find_node("HapticMouse", true, false)

func _ready():
	connect("mouse_entered", self, "on_mouse_entered")
	connect("mouse_exited", self, "on_mouse_exited")

func on_mouse_entered():
	haptic_mouse.vibrate()

func on_mouse_exited():
	pass
