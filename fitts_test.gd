extends Node2D

export(int) var amplitude = 300
export(int) var width = 50
export(int) var trials = 3

var buttons = []
var times = []
var miss_clicks = 0
var previous_tick = null

func _ready():
	for x in [-1, 1]:
		var button = preload("res://haptic_button.tscn").instance()
		add_child(button)
		button.position = get_viewport_rect().size/2 + Vector2(amplitude/2 * x, 0)
		button.scale = Vector2(width, get_viewport_rect().size.y)
		buttons.append(button)
	
	buttons[0].next = buttons[1]
	buttons[1].next = buttons[0]

func _input(event):
	if event is InputEventMouseButton and event.pressed:
		var target
		for button in buttons:
			if button.is_target:
				target = button
		
		var tick = OS.get_ticks_msec()
		if previous_tick != null:
			times.append(tick - previous_tick)
		previous_tick = tick
		
		if not target.sprite.get_rect().has_point(target.sprite.to_local(event.position)):
			miss_clicks += 1
		
		target.next.set_target()
		
		if times.size() == trials:
			print("times:", times, " miss clicks:", miss_clicks)
			get_tree().quit()
