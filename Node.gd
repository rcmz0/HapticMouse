extends Node

var is_dummy = false
var http = HTTPClient.new()
onready var audio_stream_player = get_node('AudioStreamPlayer')

func _ready():
	var core_props_file_name = {
		"OSX": "//Library/Application Support",
		"X11": OS.get_environment("HOME") + "/.wine/drive_c/ProgramData/SteelSeries",
		"Windows": "%PROGRAMDATA%/SteelSeries",
	}[OS.get_name()] + "/SteelSeries Engine 3/coreProps.json"
	
	var core_props_file = File.new()
	core_props_file.open(core_props_file_name, File.READ)
	
	if not core_props_file.is_open():
		is_dummy = true
		return
	
	var core_props_json = parse_json(core_props_file.get_as_text())
	var address = core_props_json["address"].split(":")[0]
	var port = int(core_props_json["address"].split(":")[1])
	
	http.connect_to_host(address, port)
	while http.get_status() == HTTPClient.STATUS_CONNECTING:
		http.poll()

	var bind_game_event = {
		"game": "HAPTIC_MOUSE_DEMO",
		"event": "HAPTIC_FEEDBACK",
		"value_optional": true,
		"handlers": [
			{
				"device-type": "tactile",
				"zone": "one",
				"mode": "vibrate",
				"pattern": [
					{
						"type": "ti_predefined_strongclick_100"
					}
				]	
			}
		]
	}
	http.request(
		HTTPClient.METHOD_POST, 
		"/bind_game_event", 
		["Content-Type: application/json"], 
		to_json(bind_game_event))
	while http.get_status() == HTTPClient.STATUS_REQUESTING:
		http.poll()
	http.read_response_body_chunk()
	
	vibrate()

func vibrate():
	audio_stream_player.play()
	if not is_dummy:
		var game_event = {
			"game": "HAPTIC_MOUSE_DEMO",
			"event": "HAPTIC_FEEDBACK",
			"data": {}
		}
		http.request(
			HTTPClient.METHOD_POST, 
			"/game_event", 
			["Content-Type: application/json"], 
			to_json(game_event))
		while http.get_status() == HTTPClient.STATUS_REQUESTING:
			http.poll()
		http.read_response_body_chunk()
